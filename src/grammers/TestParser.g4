parser grammar TestParser;

options { tokenVocab=TestLexer; }

program:
    arrayDefinition+
;

arrayDefinition:
    ARRAY OPENED_BRACES arrayBody CLOSED_BRACES
;

arrayBody
    : (ARRAY_STRING_VALUES COMMA)* ARRAY_STRING_VALUES #arrayString
    | (ARRAY_NUMERIC_VALUES COMMA)* ARRAY_NUMERIC_VALUES #arrayNumeric
;