lexer grammar TestLexer;

ARRAY:
    'array'
;

OPENED_BRACES:
    '(' -> pushMode(ARRAY_MODE)
;

WHITESPACE
    : [ \t\r\n] -> channel(HIDDEN)
;

mode ARRAY_MODE;

CLOSED_BRACES:
    ')' -> popMode
;

COMMA:
    ','
;

ARRAY_STRING_VALUES:
    [a-zA-Z]+
;

ARRAY_NUMERIC_VALUES:
    [0-9]+
;

