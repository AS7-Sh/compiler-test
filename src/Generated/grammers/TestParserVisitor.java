// Generated from java-escape by ANTLR 4.11.1
package Generated.grammers;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TestParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TestParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TestParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(TestParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link TestParser#arrayDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayDefinition(TestParser.ArrayDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayString}
	 * labeled alternative in {@link TestParser#arrayBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayString(TestParser.ArrayStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayNumeric}
	 * labeled alternative in {@link TestParser#arrayBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayNumeric(TestParser.ArrayNumericContext ctx);
}