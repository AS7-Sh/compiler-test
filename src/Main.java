import AST.Program.Program;
import AST.TestVisitor;
import Generated.grammers.TestLexer;
import Generated.grammers.TestParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {
    public static void main(String[] args) throws IOException {
        String source = "Files/input.txt";
        CharStream cs = fromFileName(source);
        TestLexer lexer = new TestLexer(cs);
        CommonTokenStream token = new CommonTokenStream(lexer);

        TestParser parser = new TestParser(token);
        ParseTree tree = parser.program();
        TestVisitor firstScan = new TestVisitor();
        Program program = (Program) firstScan.visit(tree);
        System.out.println(program);
    }
}