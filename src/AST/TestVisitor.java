package AST;

import AST.Array.IArray;
import AST.Array.NumericArray;
import AST.Array.StringArray;
import AST.Program.Program;
import Generated.grammers.TestParser;
import Generated.grammers.TestParserBaseVisitor;

public class TestVisitor extends TestParserBaseVisitor {
    @Override
    public Program visitProgram(TestParser.ProgramContext ctx) {
        Program program = new Program();
        for (int i = 0; i < ctx.arrayDefinition().size(); i++) {
            program.addChild(visitArrayDefinition(ctx.arrayDefinition().get(i)));
        }
        return program;
    }

    @Override
    public IArray visitArrayDefinition(TestParser.ArrayDefinitionContext ctx) {
        return (IArray) visit(ctx.arrayBody());
    }

    @Override
    public IArray visitArrayString(TestParser.ArrayStringContext ctx) {
        StringArray stringArray = new StringArray();
        for (int i = 0; i < ctx.ARRAY_STRING_VALUES().size(); i++) {
            stringArray.addChild(ctx.ARRAY_STRING_VALUES().get(i).toString());
        }
        return stringArray;
    }

    @Override
    public IArray visitArrayNumeric(TestParser.ArrayNumericContext ctx) {
        NumericArray numericArray = new NumericArray();
        for (int i = 0; i < ctx.ARRAY_NUMERIC_VALUES().size(); i++) {
            numericArray.addChild(Integer.parseInt(ctx.ARRAY_NUMERIC_VALUES().get(i).toString()));
        }
        return numericArray;
    }
}
