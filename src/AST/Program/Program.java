package AST.Program;

import AST.Array.IArray;

import java.util.ArrayList;

public class Program {
    private final ArrayList<IArray> children;

    public Program() {
        this.children = new ArrayList<>();
    }

    public void addChild(IArray child) {
        this.children.add(child);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (IArray child : this.children) {
            stringBuilder.append(child);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
