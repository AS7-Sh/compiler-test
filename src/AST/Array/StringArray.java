package AST.Array;

import java.util.ArrayList;

public class StringArray implements IArray {
    private final ArrayList<String> strings;

    public StringArray() {
        this.strings = new ArrayList<>();
    }

    public void addChild(String text) {
        this.strings.add(text);
    }

    @Override
    public String toString() {
        return "StringArray{" +
                "strings=" + strings +
                '}';
    }
}
