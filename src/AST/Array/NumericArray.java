package AST.Array;

import java.util.ArrayList;

public class NumericArray implements IArray {
    private final ArrayList<Integer> numbers;

    public NumericArray() {
        this.numbers = new ArrayList<>();
    }

    public void addChild(int number) {
        this.numbers.add(number);
    }

    @Override
    public String toString() {
        return "NumericArray{" +
                "numbers=" + numbers +
                '}';
    }
}
